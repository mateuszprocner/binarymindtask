<?php
namespace BlogBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use BlogBundle\Entity\Post;
use BlogBundle\Form\PostType;

class PostManager
{

	private $em;
	private $formFactory;

	public function __construct(\Doctrine\ORM\EntityManager $em, $formFactory) {
		$this->em = $em;
		$this->formFactory = $formFactory;
	}

    public function storePost(Request $request, \AppBundle\Entity\User $user)
    {
    	$post = new Post();

        $post->setUser($user);
        $post->setCreated(new \DateTime());

        $form = $this->formFactory->create(PostType::class, $post);	
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->em->persist($post);
            $this->em->flush();
             return true;
        }

        return false;
    }

}