<?php
namespace BlogBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use BlogBundle\Entity\Post;
use BlogBundle\Entity\Comment;
use BlogBundle\Form\CommentType;

class CommentManager
{

	private $em;
	private $formFactory;

	public function __construct(\Doctrine\ORM\EntityManager $em, $formFactory) {
		$this->em = $em;
		$this->formFactory = $formFactory;
	}

    public function storeComment(Request $request, \AppBundle\Entity\User $user)
    {
    	$comment = new Comment();

        $post = $this->em->getRepository('BlogBundle:Post')->findById($request->request->get('post'));

        
        $comment->setUser($user);
        $comment->setCreated(new \DateTime());
        $comment->setContent($request->request->get('content'));
        $comment->setPost($post);

        $this->em->persist($comment);
        $this->em->flush();
        return true;

    }

    // ...
}