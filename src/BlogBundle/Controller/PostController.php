<?php

namespace BlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BlogBundle\Entity\Post;
use BlogBundle\Form\PostType;
use BlogBundle\Form\CommentType;
use BlogBundle\Event\PostCreated;

class PostController extends Controller
{
    /**
     * @Route("/", name="postslist")
     */
    public function indexAction(Request $request)
    {
         $posts = $this->getDoctrine()
        ->getRepository('BlogBundle:Post')
        ->findAllOrderedByDate();

        
        return $this->render('post/list.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * @Route("/post/create", name="createpost")
     */
    public function create() {

        $post = new Post();

        $form = $this->createForm(PostType::class, $post);
        
        return $this->render('post/create.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'form' => $form->createView()
        ]);

    }

     /**
     * @Route("/post/{id}/show", name="showpost")
     */
    public function show($id) {

        $post = $this->getDoctrine()
        ->getRepository('BlogBundle:Post')
        ->findById($id);

        $form = $this->createForm(CommentType::class, $post);

        return $this->render('post/show.html.twig', [
            'post' => $post,
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/post/store", name="storepost")
     */
    public function store(Request $request) {

        $user = $this->getUser();


        $postManager = $this->get('post_manager');
        $result = $postManager->storePost($request, $user);

        if($result) {
            $event = new PostCreated();
            $this->get('event_dispatcher')->dispatch('event.post_created', $event);
            return $this->redirect($this->generateUrl('postslist'));
            

        
        }

    }
}
