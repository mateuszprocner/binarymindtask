<?php

namespace BlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BlogBundle\Entity\Post;
use BlogBundle\Form\PostType;
use BlogBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\JsonResponse;
use BlogBundle\Event\CommentCreated;

class CommentController extends Controller
{

    /**
     * @Route("/comment/store", name="storecomment")
     */
    public function store(Request $request) {

        $user = $this->getUser();
        if($user) {

            $commentManager = $this->get('comment_manager');
            $result = $commentManager->storeComment($request, $user);
            $event = new CommentCreated();
            $this->get('event_dispatcher')->dispatch('event.comment_created', $event);
            return new JsonResponse();
        } else {
            $response = new JsonResponse();
            $response->setStatusCode(403);
            return $response;
        }
    }

    /**
     * @Route("/comment/list/{id}", name="listcomments")
     */
    public function list($id) {
        $comments = $this->getDoctrine()
            ->getRepository('BlogBundle:Comment')
            ->findForPostOrderedByDate($id);

        
        return $this->render('comment/list.html.twig', [
            'comments' => $comments
        ]);
    }
}
