<?php
namespace BlogBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use BlogBundle\Event\PostCreated;

class CreatePostEventListener implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return array(
            'event.post_created' => 'handler',
        );
    }

    public function handler(PostCreated $event)
    {
       return true;
    }

}

