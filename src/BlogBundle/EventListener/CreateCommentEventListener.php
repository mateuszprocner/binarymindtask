<?php
namespace BlogBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use BlogBundle\Event\CommentCreated;

class CreateCommentEventListener implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return array(
            'event.comment_created' => 'handler',
        );
    }

    public function handler(CommentCreated $event)
    {
       return true;
    }

}

